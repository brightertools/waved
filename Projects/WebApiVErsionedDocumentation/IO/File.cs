﻿using System;
using System.IO;
using System.Text;

namespace WebApiVErsionedDocumentation.IO
{
    public static class File
    {
        /// <summary>
        /// Gets the contents of specified file to a string
        /// </summary>
        /// <param name="filename"> </param>
        /// <param name="throwExceptionIfFileDoesNotExist"> </param>
        /// <returns></returns>
        public static string ToString(string filename, bool throwExceptionIfFileDoesNotExist = false)
        {
            var result = new StringBuilder();

            if (System.IO.File.Exists(filename))
            {
                using (var streamReader = new StreamReader(filename))
                {
                    String line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        result.Append(String.Concat(line, Environment.NewLine));
                    }
                }
            }
            else
            {
                if (throwExceptionIfFileDoesNotExist)
                {
                    throw new IOException("The specified file does not exist");
                }
            }

            return result.ToString();
        }
    }
}