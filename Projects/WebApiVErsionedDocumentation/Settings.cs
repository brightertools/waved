﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace WebApiVErsionedDocumentation
{
    public static class Settings
    {
        public static WebApiDocumentationSetting GetWebApiDocumentationSettings(string apiName, int? version)
        {
            var name = String.IsNullOrWhiteSpace(apiName) ? WebApiDocumentationDefaultApi() : apiName;
            var settings = WebApiDocumentationSettings().FirstOrDefault(x => String.Equals(x.ApiControllerName.Replace("Controller", String.Empty), name.Replace("Controller", String.Empty), StringComparison.InvariantCultureIgnoreCase));
            if (settings == null)
            {
                return new WebApiDocumentationSetting
                {
                    ApiControllerName = String.Empty,
                    OldestVersion = 1,
                    LatestVersion = 1,
                    DefaultVersion = 1
                };
            }

            if (version.HasValue)
            {
                if (version >= settings.OldestVersion && version <= settings.LatestVersion)
                {
                    settings.DefaultVersion = version.Value;
                }
            }

            return settings;
        }

        /// <summary>
        /// Returns a list of Web Api Documentation Settings for each Api webconfig settings in the following format:
        /// Key: WebApiDocumentation.Setting Value = A.B.C.D
        /// Where: A = ApiName
        ///        B = OldestVersion (oldest version available to view)
        ///        C = LatestVersion (latest version available to view)
        ///        D = DefaultVersion (default version to view)
        /// </summary>
        /// <returns>List of WebApiDocumentationSetting</returns>
        public static IEnumerable<WebApiDocumentationSetting> WebApiDocumentationSettings()
        {
            var result = new List<WebApiDocumentationSetting>();
            foreach (var key in ConfigurationManager.AppSettings.AllKeys.Where(x => x.StartsWith("WebApiDocumentation.Setting")))
            {
                var value = ConfigurationManager.AppSettings[key];
                var settingValues = value.Split('.');
                if (settingValues.Length == 4)
                {
                    try
                    {
                        var setting = new WebApiDocumentationSetting
                        {
                            ApiControllerName = settingValues[0].Trim(),
                            OldestVersion = Int32.Parse(settingValues[1]),
                            LatestVersion = Int32.Parse(settingValues[2]),
                            DefaultVersion = Int32.Parse(settingValues[3])
                        };
                        result.Add(setting);
                    }
                    // ReSharper disable once EmptyGeneralCatchClause
                    catch
                    {
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Default Api to show on documentation page (Api Controller Name, eg FileV1, FileV1Controller, can be empty if api contoller is called V1)
        /// </summary>
        /// <returns></returns>
        public static string WebApiDocumentationDefaultApi()
        {
            return (String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["WebApiDocumentation.DefaultApiController"]) ? String.Empty : ConfigurationManager.AppSettings["WebApiDocumentation.DefaultApiController"]);
        }

        /// <summary>
        /// Company name, used for copyright text
        /// </summary>
        /// <returns></returns>
        public static string WebApiDocumentationCompanyName()
        {
            return (String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["WebApiDocumentation.CompanyName"]) ? String.Empty : ConfigurationManager.AppSettings["WebApiDocumentation.CompanyName"]);
        }

        /// <summary>
        /// Support email address
        /// </summary>
        /// <returns></returns>
        public static string WebApiDocumentationSupportEmailAddress()
        {
            return (String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["WebApiDocumentation.SupportEmailAddress"]) ? String.Empty : ConfigurationManager.AppSettings["WebApiDocumentation.SupportEmailAddress"]);
        }

        /// <summary>
        /// Logo Image and Link Url in the format: ImageUrl|LinkUrl
        /// </summary>
        /// <returns></returns>
        public static Tuple<string, string> WebApiDocumentationHeaderLogo()
        {
            var headerLogoSetting = String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["WebApiDocumentation.HeaderLogoUrl"]) ? String.Empty : ConfigurationManager.AppSettings["WebApiDocumentation.HeaderLogoUrl"];

            if (headerLogoSetting.Contains("|"))
            {
                var parts = headerLogoSetting.Split(new[] {'|'}, StringSplitOptions.RemoveEmptyEntries);
                return new Tuple<string, string>(parts[0], parts[1]);
            }

            return null;
        }

        /// <summary>
        /// Root Url of API
        /// </summary>
        /// <returns></returns>
        public static string WebApiDocumentationRootUrl()
        {
            return (String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["WebApiDocumentation.RootUrl"]) ? String.Empty : ConfigurationManager.AppSettings["WebApiDocumentation.RootUrl"]);
        }

        /// <summary>
        /// Returns a list of additional links in the format (description|url;desciption|url) which are rendered at the top of the help page
        /// </summary>
        /// <returns></returns>
        public static List<Tuple<string, string>> WebApiDocumentationAdditionalLinks()
        {
            var links = ConfigurationManager.AppSettings["WebApiDocumentation.AdditionalLinks"].Split(new[] {';', ','}, StringSplitOptions.RemoveEmptyEntries).ToList();
            return (from link in links where link.Contains("|") select link.Split(new[] {'|'}, StringSplitOptions.RemoveEmptyEntries) into linkParts select new Tuple<string, string>(linkParts[0], linkParts[1])).ToList();
        }
    }
}