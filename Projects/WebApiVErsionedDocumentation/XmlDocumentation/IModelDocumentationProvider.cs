﻿using System;
using System.Reflection;

namespace WebApiVErsionedDocumentation.XmlDocumentation
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}