﻿using System.Web.Http;
using System.Web.Http.Description;

namespace WebApiVErsionedDocumentation.XmlDocumentation
{
    public static class HttpConfigurationExtensions
    {
        /// <summary>
        /// Sets the documentation provider for help page.
        /// </summary>
        /// <param name="config">The <see cref="HttpConfiguration"/>.</param>
        /// <param name="documentationProvider">The documentation provider.</param>
        public static void SetDocumentationProvider(this HttpConfiguration config, IDocumentationProvider documentationProvider)
        {
            config.Services.Replace(typeof (IDocumentationProvider), documentationProvider);
        }
    }
}