﻿using System.Web;
using System.Web.Http;

namespace WebApiVErsionedDocumentation.XmlDocumentation
{
    /// <summary>
    /// WebApiXmlDocumentationConfig
    /// </summary>
    public static class Config
    {
        public static void Register(HttpConfiguration config)
        {
            config.SetDocumentationProvider(new XmlDocumentationProvider(HttpContext.Current.Server.MapPath("~/App_Data/Documentation.xml")));
        }
    }
}