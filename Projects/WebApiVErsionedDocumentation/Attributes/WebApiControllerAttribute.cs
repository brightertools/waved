﻿using System;

namespace WebApiVErsionedDocumentation.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class WebApiControllerAttribute : Attribute
    {
        public string Name { get; private set; }
        public string Version { get; private set; }
        public bool IsBeta { get; private set; }
        public bool IsDiscontinued { get; private set; }

        public WebApiControllerAttribute(string name, string version)
        {
            Name = name;
            Version = version;
            IsBeta = false;
            IsDiscontinued = false;
        }

        public WebApiControllerAttribute(string name, string version, bool isBeta)
        {
            Name = name;
            Version = version;
            IsBeta = isBeta;
            IsDiscontinued = false;
        }

        public WebApiControllerAttribute(string name, string version, bool isBeta, bool isDiscontinued)
        {
            Name = name;
            Version = version;
            IsBeta = isBeta;
            IsDiscontinued = isDiscontinued;
        }
    }
}