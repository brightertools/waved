namespace WebApiVErsionedDocumentation
{
    public class WebApiDocumentationSetting
    {
        public string ApiControllerName { get; set; }
        public int OldestVersion { get; set; }
        public int LatestVersion { get; set; }
        public int DefaultVersion { get; set; }
    }
}