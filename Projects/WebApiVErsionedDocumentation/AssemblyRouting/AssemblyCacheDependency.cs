﻿using System.IO;
using System.Reflection;
using System.Web.Caching;

namespace WebApiVErsionedDocumentation.AssemblyRouting
{
	class AssemblyCacheDependency : CacheDependency
	{
		public AssemblyCacheDependency(Assembly assembly)
		{
			SetUtcLastModified(File.GetCreationTimeUtc(assembly.Location));
		}
	}
}
