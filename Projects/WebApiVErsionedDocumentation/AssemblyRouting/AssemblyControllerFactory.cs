﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApiVErsionedDocumentation.AssemblyRouting
{
	class AssemblyControllerFactory : DefaultControllerFactory
	{
		private readonly IDictionary<String, Type> _controllerTypes;

		public AssemblyControllerFactory(Assembly assembly)
		{
			_controllerTypes = assembly.GetExportedTypes().Where(x => typeof(IController).IsAssignableFrom(x) && (x.IsInterface == false) && (x.IsAbstract == false)).ToDictionary(x => x.Name, x => x);
		}

		public override IController CreateController(RequestContext requestContext, String controllerName)
		{
			var controller = base.CreateController(requestContext, controllerName);

			if (controller == null)
			{
				var controllerType = _controllerTypes.Where(x => x.Key == $"{controllerName}Controller").Select(x => x.Value).SingleOrDefault();
				var controllerActivator = DependencyResolver.Current.GetService(typeof (IControllerActivator)) as IControllerActivator;

				if (controllerType != null)
				{
					if (controllerActivator != null)
					{
						controller = controllerActivator.Create(requestContext, controllerType);
					}
					else
					{
						controller = Activator.CreateInstance(controllerType) as IController;
					}
				}
			}

			return (controller);
		}
	}
}
