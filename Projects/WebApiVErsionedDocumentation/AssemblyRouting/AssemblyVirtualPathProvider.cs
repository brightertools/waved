﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Caching;
using System.Web.Hosting;
using System.Web.Mvc;

namespace WebApiVErsionedDocumentation.AssemblyRouting
{
	class AssemblyVirtualPathProvider : VirtualPathProvider
	{
		private readonly Assembly _assembly;
		private readonly IEnumerable<VirtualPathProvider> _providers;

		public AssemblyVirtualPathProvider(Assembly assembly)
		{
			var engines = ViewEngines.Engines.OfType<VirtualPathProviderViewEngine>().ToList();

			_providers = engines.Select(x => x.GetType().GetProperty("VirtualPathProvider", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(x, null) as VirtualPathProvider).Distinct().ToList();
			_assembly = assembly;
		}

		public override CacheDependency GetCacheDependency(String virtualPath, IEnumerable virtualPathDependencies, DateTime utcStart)
		{
		    if (FindFileByPath(CorrectFilePath(virtualPath)) != null)
			{
				return (new AssemblyCacheDependency(_assembly));
			}
		    return (base.GetCacheDependency(virtualPath, virtualPathDependencies, utcStart));
		}

	    public override Boolean FileExists(String virtualPath)
		{
			if (_providers.Any(provider => provider.FileExists(virtualPath)))
			{
			    return (true);
			}

			var exists = FindFileByPath(CorrectFilePath(virtualPath)) != null;

			return (exists);
		}

		public override VirtualFile GetFile(String virtualPath)
		{
		    foreach (var provider in _providers)
			{
				var file = provider.GetFile(virtualPath);

				if (file != null)
				{
					try
					{
						file.Open();
						return (file);
					}
                    // ReSharper disable once EmptyGeneralCatchClause
					catch
					{
					}
				}
			}

			var resourceName = FindFileByPath(CorrectFilePath(virtualPath));

			return (new AssemblyVirtualFile(virtualPath, _assembly, resourceName));
		}

	    private String FindFileByPath(String virtualPath)
		{
			var resourceNames = _assembly.GetManifestResourceNames();

			return (resourceNames.SingleOrDefault(r => r.EndsWith(virtualPath, StringComparison.OrdinalIgnoreCase)));
		}

	    private String CorrectFilePath(String virtualPath)
		{
			return (virtualPath.Replace("~", String.Empty).Replace('/', '.'));
		}
	}
}