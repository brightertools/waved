﻿using System;
using System.IO;
using System.Reflection;
using System.Web.Hosting;

namespace WebApiVErsionedDocumentation.AssemblyRouting
{
	class AssemblyVirtualFile : VirtualFile
	{
		private readonly Assembly _assembly;
		private readonly String _resourceName;

		public AssemblyVirtualFile(String virtualPath, Assembly assembly, String resourceName) : base(virtualPath)
		{
			_assembly = assembly;
			_resourceName = resourceName;
		}

		public override Stream Open()
		{
            // ReSharper disable once AssignNullToNotNullAttribute
			return (_assembly.GetManifestResourceStream(_resourceName));
		}
	}
}
