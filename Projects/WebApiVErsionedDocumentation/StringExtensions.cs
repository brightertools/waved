﻿using System.Text.RegularExpressions;

namespace WebApiVErsionedDocumentation
{
    /// <summary>
    /// String Extensions
    /// </summary>
    /// <returns></returns>
    public static class StringExtensions
    {
        /// <summary>
        /// Separates a string in pascal case into separate words.
        /// Set useNonBreakingSpace to return string with html non breaking spaces
        /// </summary>
        /// <param name="value"></param>
        /// <param name="useNonBreakingSpace"></param>
        /// <returns></returns>
        public static string SplitPascalCaseWords(this string value, bool useNonBreakingSpace = false) { return Regex.Replace(value, "([A-Z][A-Z]*)", " $1", RegexOptions.Compiled).Trim().Replace(" ", useNonBreakingSpace ? "&nbsp;" : " "); }
    }
}