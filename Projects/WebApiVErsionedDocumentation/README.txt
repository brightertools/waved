WebApiVErsionedDocumentation (WAVED)

This is an open source project that creates versioned ASP.NET Web API documentation based on comments within the API controller.

This supports multiple versions of multiple Web Api's in a project.

More at: https://waved.codeplex.com/