﻿using System;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using WebApiVErsionedDocumentation.Attributes;
using WebApiVErsionedDocumentation.Models;

namespace WebApiVErsionedDocumentation.Controllers
{
    public class ApiDocumentationController : Controller
    {
        [System.Web.Mvc.HttpGet]
        public ActionResult Index(string name, int? version)
        {
            var apiSettings = Settings.GetWebApiDocumentationSettings(name, version);
            // ReSharper disable once Mvc.ViewNotResolved
            return View(CreateApiDocumentationModel(apiSettings.ApiControllerName, apiSettings.DefaultVersion, apiSettings.OldestVersion, apiSettings.LatestVersion));
        }

        private static ApiDocumentationModel CreateApiDocumentationModel(string apiName, int version, int oldestVersion, int latestVersion)
        {
            var configuration = GlobalConfiguration.Configuration;
            var apiDescriptions = configuration.Services.GetApiExplorer().ApiDescriptions;

            var apiControllerVersionNameLookup = String.Concat(apiName, "V", version);

            var viewModel = new ApiDocumentationModel
            {
                RootUrl = Settings.WebApiDocumentationRootUrl(),
                Name = apiName.SplitPascalCaseWords(),
                DocumentationProvider = configuration.Services.GetDocumentationProvider(),
                ApiDescriptions =
                    apiDescriptions.Where(
                        x => x.ActionDescriptor.ControllerDescriptor.ControllerName.StartsWith(
                            apiControllerVersionNameLookup, StringComparison.InvariantCultureIgnoreCase))
                        .ToList(),
                Version = "Unknown",
                OldestVersion = oldestVersion,
                LatestVersion = latestVersion,
                IsBeta = true,
                CompanyName = Settings.WebApiDocumentationCompanyName(),
                SupportEmailAddress = Settings.WebApiDocumentationSupportEmailAddress(),
                HeaderLogo = Settings.WebApiDocumentationHeaderLogo(),
                AdditionalLinks = Settings.WebApiDocumentationAdditionalLinks()
            };

            if (viewModel.ApiDescriptions.Count > 0)
            {
                var attrributes =
                    viewModel.ApiDescriptions[0].ActionDescriptor.ControllerDescriptor
                        .GetCustomAttributes<WebApiControllerAttribute>();

                if (attrributes.Count > 0)
                {
                    viewModel.Version = attrributes[0].Version;
                    viewModel.IsBeta = attrributes[0].IsBeta;
                    viewModel.IsDiscontinued = attrributes[0].IsDiscontinued;
                }
            }

            viewModel.ReleaseNotes = IO.File.ToString(Path.Combine(HostingEnvironment.MapPath("~") ?? String.Empty, String.Concat("RELEASE-", apiName, ".txt"))).Replace(Environment.NewLine, "<br />");

            viewModel.ReleaseNotes = String.IsNullOrEmpty(viewModel.ReleaseNotes)
                ? "<p>Release notes are not available.</p>"
                : viewModel.ReleaseNotes;

            return viewModel;
        }
    }
}
