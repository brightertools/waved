﻿using System;
using System.Collections.Generic;
using System.Web.Http.Description;

namespace WebApiVErsionedDocumentation.Models
{
    public class ApiDocumentationModel
    {
        public string Name { get; set; }
        public string RootUrl { get; set; }
        public Tuple<string,string> HeaderLogo { get; set; }
        public string FooterLogoUrl { get; set; }
        public string CompanyName { get; set; }
        public string Version { get; set; }
        public int OldestVersion { get; set; }
        public int LatestVersion { get; set; }
        public bool IsBeta { get; set; }
        public bool IsDiscontinued { get; set; }
        public string SupportEmailAddress { get; set; }
        public IDocumentationProvider DocumentationProvider { get; set; }
        public List<ApiDescription> ApiDescriptions { get; set; }
        public string ReleaseNotes { get; set; }
        public List<Tuple<string, string>> AdditionalLinks { get; set; }
    }
}